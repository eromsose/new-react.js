import React, { useState } from 'react';

function TransactionStatus() {
  const [email, setEmail] = useState('');
  const [Amouunt, setAmount] = useState('');
  const [transactionStatus, setTransactionStatus] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Simulate API call with placeholder data (replace with actual API call)
    const response = await fetch('https://api.kochure.com/v1/sup/bank-withdrawals/re-initiate', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email, Amount }),
    });

    if (!response.ok) {
      // Handle errors (e.g., invalid credentials, server error)
      console.error('Error:', response.statusText);
      setTransactionStatus('An error occured. Please try again.');
      return;
    }

    const data = await response.json();

    // Simulate parsing the response data (replace with actual data structure)
    const isSuccessful = data.success;
    const transactionMessage = isSuccessful ? 'Transaction Successful' : 'Transaction Failed';

    setTransactionStatus(transactionMessage);
  };

  return (
    <div>
      <h1>Bank Transaction Status</h1>
      <form onSubmit={handleSubmit}>
        <label htmlFor="email">Email:</label>
        <input type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
        <label htmlFor="Amount">Amount:</label>
        <input type="Amount" id="Amount" value={Amount} onChange={(e) => setAmount(e.target.value)} required />
        <button type="submit">Check Status</button>
      </form>
      {transactionStatus && <p>{transactionStatus}</p>}
    </div>
  );
}

export default TransactionStatus;
